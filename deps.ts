// -- Immediate exports & imports --
import * as path from 'https://deno.land/std@0.207.0/path/mod.ts';
export { path };


// -- Computed exports --
export const DIRNAME = path.dirname(path.fromFileUrl(import.meta.url));


// -- Standard exports --
export type { default as Site } from 'https://deno.land/x/lume@v1.19.3/core/site.ts';
export { default as Nunjucks } from 'npm:nunjucks';
export { default as MarkdownIt } from 'npm:markdown-it';

export {
  Document,
  DOMParser,
  type Element,
} from 'https://deno.land/x/deno_dom@v0.1.43/deno-dom-wasm.ts';

export { ReflectionKind } from 'https://raw.githubusercontent.com/TypeStrong/typedoc/v0.25.3/src/lib/models/reflections/kind.ts';
