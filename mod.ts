import type { Site } from "./deps.ts";
import { Controller, type Options } from './trdoc/src/Controller.ts';

export default function(config?: Options) {
  return (site: Site) => {
    const controller = new Controller(config);

    // Adding a custom page loader for `*.typedoc.json` files
    site.loadPages(['.typedoc.json'], async (path: string) => {
      const content = await Deno.readTextFile(path);
      return controller.jsonPageLoader(content);
    });

    // Copying the main css file over to the desired destination (or root)
    site.remoteFile(controller.cssDestFile, controller.cssFilePath);
    site.copy(controller.cssDestFile);

    site.component('reference', {
      name: 'doc',
      render: controller.build
    });
    site.component('reference', {
      name: 'main',
      render: controller.buildMain
    });
    site.component('reference', {
      name: 'overview',
      render: controller.buildOverview
    });
    site.component('reference', {
      name: 'outline',
      render: controller.buildOutline
    });
  }
}
